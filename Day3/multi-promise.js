const {readFile} = require("fs/promises")

function readMultiFiles(){
    console.log("Start")

    function printContent(buffer){
        console.log(buffer.toString());
    }

    function errorHandler(error){
        console.log("Oh no you got this error " + error.message)
    }

    const smallFilePromise = readFile("ello.txt"); // JS is not waiting around for the file
    // to become fully loaded into our program
    const largeFilePromise = readFile("large.txt");
    smallFilePromise.then(printContent).catch(errorHandler);
    largeFilePromise.then(printContent);

    console.log("End")
}

readMultiFiles()
// start
// end
// small
// large

async function readManyFiles(){
    console.log("Begin");
    try {
        const smallBuffer = await readFile("ello.txt");
        const largeBuffer = await readFile("large.txt");
        console.log(smallBuffer.toString())
        console.log(largeBuffer.toString())
    } catch (error) {
        console.log("Oh no you got this error " + error.message)
    }

    console.log('Finished');
}
//readManyFiles();