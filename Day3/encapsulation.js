
function encapsulate(paramvalue){
    let value = paramvalue;

    function setValue(v){
        value = v;
    }

    function getValue(){
        return value;
    }
    return {getValue:getValue,setValue}// You do not have to define the properties on an object
    // if you want to keep the same variable name
}
const obj = encapsulate()
obj.setValue(100)
console.log(obj.getValue())