// ES6 feature
class Person{

    // A class constructor function builds the object
    // must be called constructor
    // only one constructor
    // static variable is a property attached the the Class itself not the objects
    // that it creates
    static counter = 0;

    constructor(fname,lname,age){
        // object properties can be defined in the constructor
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        Person.counter += 1;
    }

    describeSelf(){
        console.log(`Hello my name is ${this.fname}`)
    }
}

const adam = new Person('Adam',"Ranieri",19);
adam.describeSelf()
console.log(adam)
console.log(Person.counter)