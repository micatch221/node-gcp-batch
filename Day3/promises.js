// JS is an asynchronous, single-threaded, event driven language
// JS can only do ONE thing at a time
// Other languages like Java can run multiple things in parallel at the some time

// A lot of times your code has to INTERACT with something OUTSIDE of your JS program
// a lot of these functions return promises
// common things
// reading a file on your computer
// Talking to a database
// Making HTTP requests
// It would be really inefficient of JS just stood twiddling it's thumbs while waiting for things out of it's control to finish

const {readFile} = require("fs/promises");

function getFile(){
    console.log("Started get File")
    // readFile does not return a file
    // it returns a promise
    // a promise is an empty object that will eventually hold a value
    const filePromise = readFile("hello.txt") // readFile takes in the path to a file you want to read
    
    const printFileContent = function(buffer){
        console.log(buffer.toString())
    }

    // when the promise does become real 'then' pass it into this function
    filePromise.then(printFileContent);
    // JS does not wait around
    console.log("Ended get File")
}


//ES6 async await
// you can write asynchronous code as IF it was synchronous 
// async is a keyword that allows you to use await
async function getFile2(){
    console.log("Start")
    const buffer = await readFile("hello.txt")
    // Tell JS to "wait" for the promise to become real then continue
    console.log(buffer.toString())
    console.log("Finish")
}

getFile2()