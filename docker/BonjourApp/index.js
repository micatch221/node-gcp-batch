const express = require('express');

const app = express()

let counter = 0;

app.get('/bonjour', (req,res)=>{
    console.log('Hello I just logged!!!')// In Docker anything printed to stdout or stderr is stored in docker logs you can view
    res.send('Bonjour !!!'+ ++counter)
})

const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>console.log('Application Started'))
