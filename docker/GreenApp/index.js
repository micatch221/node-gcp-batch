const express = require('express');
const app = express();

app.get('/username', (req,res)=>{
    const username = process.env.USERNAME;
    res.send("The username is " + username);
})

app.get('/password', (req,res)=>{
    const password = process.env.PASSWORD;
    res.send('the password is '+ password);
})

app.listen(3000,()=>console.log('App Started'))