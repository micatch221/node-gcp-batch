# Project 2
## My-Town Issue Review Board

You will be building a **City Issue Review System CIRS** for a local city government. The primary duty of the review board is to address pressing concerns and issues in the community. They want an application that makes it as easy as possible for constituents to anonymously post issues. They also need to analyze and review issues to find common threads and possible solutions. The review board will use this data to arrange town hall meetings. These meetings are open to the public. ***Due October 4th***.  This is a team project and you will be working in teams of 4.

### User Stories

|As a| I want to | So That|
|----|-----------|--------|
|Constituent| Submit an issue | the review board can address the issue|
|Constituent| View Town Hall Meetings and details | I can attend meetings important to me| 
|Council Member| Review all issues | no one's issues are missed|
|Council Member| Review issues by type | the most prevalent issues are addresed first|
|Council Member| Mark issue as reviewed | to filter out issues that will not be addressed or solved already|
|Council Member| Highlight an issue| Other council memebers see it|
|Council Member| Create Town Meetings | constituents can air their thoughts|
|Council Member| Edit Meetings | I can change time/topics to best reflect what is being covered|

### Technical Requirements
- All services must be containerized.
    - The images should be stored on GCR
- The entire appliction must be running on GCP
    - This should be a single project.
    - This means you will have to assign IAM permissions to other members.
- The application should generate logs for .
    - Issue submitted
    - Town Hall meeting created
    - Town Hall meeting edited
    - All errors
- These logs should get routed into BigQuery.
- All code must be in GitLab
- All services other than the Issue-Ingestion service should run on a kubenetes Cluster

# Services
## Issue-Ingestion
- This service will be responsible for processing incoming requests and publishing to pub/sub.
    - This service will use **Cloud Run** for deployment.
    - Will verify that at a minimum an issue contains the following.
        - Date of posting
        - Date of issue
        - Issue Description
        - Location
        - Type of Issue
            - Infrastructure
            - Safety
            - Public Health
            - Pollution
            - Noise/Distrubing the peace
            - Other
- Routes
    - POST /submitissue
- Should use a Terraform Script to deploy this service and generate the pub/sub topic and subscription.

## Issue-Analysis Service
- This service will be responsible analyzing issues and providing useful information
- Routes
    - GET /issues
    - GET /issues?type=something
    - GET /issues?dateposted=somedate
    - GET /issues?dateofissue=somedate
    - GET /issues?reviewed=true
    - GET /issues?highlighted=true
    - GET /issues/report
        - Returns a single JSON that contains summarized information with the following.
            - the number of each type of issue
            - number of issues reported in the last 24 hours
                - broken down by type 
            - number of issues reported in the last 7 days
                - broken down by type
            - number of issues reviewed
            - number of issess pending reviewal
        - This route also generates a report.txt that has the inforation
        - This file is stored locally
        - This will be saved to a persistent disk using PVC
- Data should be persisted in Datastore
- use workload identity to authorize the pod for datastore

#### Town-Meeting-Scheduler
- This service is responsible for creating managing meetings
- Meetings at a minimum should have the following information
    - Location
    - Time
    - Topics of discussion
- Only council members are suppposed to use the POST, PUT and delete routes.
    - You will be using a header called 'Authorization' that contains a secret password. The service will check that header.
    - In practice this would be a JWT but you can use just a text password to start.
- Routes
    - GET /meetings
    - GET /meetings/:id
    - POST /meetings
    - PUT /meetings/:id
    - DELETE /meetings/:id
- Data should be persisted in Cloud SQL
- use a kubernetes secret to save the DB credentials


