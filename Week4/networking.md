# Networking
- **IP Address**
    - Internet Protocol address
    - A unique identifier for a device connected to the internet
    - An IP address is actually a 4 8 bit quad seperated by dots usually.
        - 10110111.10101101.111001011.11100011
            - each quad has 2^8 total values for decimal values between 0-255
        - 32 bits 
        - usally seperated by dots
    - Typically you use the decmailized version of an IP address for readability
- Limitations of the IP Address system
    - 4.2 billion total IP address avaialale in the world
    - IP addresses are a bit of a scarce resource
        - Cloud providers have a large amount of these IP addresses and allocate them to the developers creating vms and databases.
        - This is why a lot of your IP addresses can change if you shut down your VM
- The **IPv4** system is the most common IP address system  
    - IPv6 is a new IP system that primarily for the IoT
    - Internet Assigned Numbers Authority orginially owned all 4.2 billion addresses and ended up giving ranges of IP addresses to telecom companies all around the world
- There used to be a class based system for assigning IP addresses
    - For instance Microsoft might get a class A IP address range that gives them a few hundred million IP addresses.
- **CIDR notation** 
    - **Classes Inter-domain Routing**
    - A newer system for defining a range of IP addresses
        - 123.98.20.0/24 
            - first part is the IP address 
            - the second part after the slash is how many significant bits there are in the IP address
            - You are defining a RANGE of IP addresses
    - 123.98.20.0/24 
        - You are selecting all IP addresses from
            - 123.98.20.0 - 123.98.20.255
    - 201.0.0.0/8
        - 201.0.0.0 - 201.255.255.255
- 34.74.90.64/28
    - 11111111.11111111.11111111.11110000 - 11111111.11111111.11111111.1111111111
    - 34.74.90.64 - 34.74.90.255
## Ports
- You can identify a computer on the internet using the IP address but you then need to access the computer via some port number.
- Reserved Port numbers
    - HTTP: 80
        - You can make HTTP requests to any port its just that HTTP default to 80
    - HTTPS : 443
    - SSH: 22
    - SMTP: 25 (email)

## SSH
- Secure Shell
- runs on port 22
- The primary way of connecting to linux machines

## VPC 
- **Virtual Private Cloud**
- Encapsualted set of IPs just for you to work with
- VPC is **Global**
- You can set up networking and firerules for a VPC
- Advantages
    - Security
        - Block traffic to computers that should not be rached

## Subnet 
- A sub section of a VPC
- Google will give a range of IP address for your project(s) in a **specfic region**
- subnet for asia-east1


## Firewall Rules
- A VPC can have have Firewall rules that you create and customize
- These rules will block or allow traffic to computers on your network
    - Block based on IP address
    - Block based on the port it is trying to access
    - Block based on Service Account
- By default any newly created network 
    - Allows traffic from anywhere to port 22 (SSH)
    - All traffic between computers on the network is allowed
    - Any other traffic to computers on the network is blocked
- Parts of a firewall rule in GCP
    - **Name**
        - Human readable description
    - **Type**
        - ingress (traffic going *into* the network)
        - egress (traffic going *out* of the the network)
    - **Target**
        - All computers on the network
        - Specific target tag
        - Service Account
    - **Filter**
        - IP Range
        - Service Account
        - specific tags
    - **Port**
    - **Action**
        - Allow (The rule is granting the permission to do something)
        - Deny (The rules prohibits)
    - **Priority**
        - number between 0 - 65,535
        - Lower numbers take precedence over higher numbers
        - if a rule with a priortity of 1000 allows traffic from anywhere on port 8000 it will override a rule with priority 5000 blocking traffic from everywhere.

# Load Balancer
- Most modern applications are very reliable because they are deployed across multiple machines.
- Redundnacy will means that if a single instance fails other instances can still host the applications.
- Web Applications tend to scale better for really large demand by splitting incoming traffic to multiple instances
- Load Balancers perform Health Checks to make sure that instances it is forwarding to are able to process requests
- External Load Balancers
    - Face the internet and forward requests to your instances
- Internal Load Balancers
    - Only in your VPC and for instances communicating with each other
