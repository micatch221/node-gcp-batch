# Google Compute Engine
- The primary service for creating VMs on google
- The vms are highly customizable

# Compute Engine
- One of the core services of GCP.
- You can create custom Virtual Machines (VMs) on the google cloud.
- These machine are just like your own computers but are hosted on GCP.
- Google can track these VMs, to do logging for you, maitnence, make backups of the disk drive, duplicate these VMs etc.
- Hosting a backend app on CE 101
    1. The VM has the appropriate software installed on it
        - node
        - npm
        - your source code
    2. Start your application on that VM
    3. The firewall for the network your VM is on is allowing traffic to that VM and port
```bash
node index.js &
ps # see all programs runin the background
disown 7331 # disown the process ID  so that the process/applicaiton keeps running even after you quit


```
## Terminology
- **Instance** : A single VM
- **Persistent Disk**: A hard drive attached to a VM
    - These are detachable
    - You can create snapshots of Persistent Disks
        - You can use a snapshot to creat new instances
- **Snapshot**: an exact copy of a Persistent disk
    - You can have as many snapshots as you want
    - You can even schedule them to be created
- **Instance Template**: Pre-saved confifuration for creating an instance
    - Very helpful for creating multiple copies of a VM
- **Instance Group**: several VMs that you group together
    - Unmanaged Instance Group
        - You manually add or remove servers to the group.
        - It gets the term unmanaged because google is not doing anything.
        - These server DO NOT have to be identical. (but should for the most part)
    - Managed Instance Group
        - All instances MUST be the same
            - All instances must be created using the same template
        - Google can automatically manage these instances.
            - Create more or less of them based on how much traffic they are recieving

    