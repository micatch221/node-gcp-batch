const {PubSub} = require('@google-cloud/pubsub');
const pubsub = new PubSub({projectId:'reva-points-tracker-ranieri'});

async function pullMessages(){
    const subscription = pubsub.subscription('suggestion-viewer-subscription');
    // when that subscription recieves a message use this callback function
    // which takes in the message parameter
    subscription.on('message', (message) => {
        const suggestion = message.data.toString();
        console.log('Got a new message ' + suggestion)
        message.ack()//Acknowledge the message. This will tell the subscription
        // that the message was successfully recieved and processed and can be removed from 
        // the subscription
    })
}

pullMessages();//Your application will hang here
// subscription.on is like app.listen in express