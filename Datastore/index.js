const {Datastore} = require('@google-cloud/datastore')
const datastore = new Datastore()

//create
async function saveEmployee(){

    // will specific what kind we are working with
    const key = datastore.key('Employee');

    const employee = {
        fname:'Sierra',
        lname:"Nicholes",
        seniority:"Senior Trainer",
        location: 'WVU',
        specialty: 'JWA'
    }

    const response = await datastore.save({key:key,data:employee})
    console.log(response)

}

//read get a single employee
async function getEmployee(){
    const key = datastore.key(['Employee',5071211717459968]);
    const response = await datastore.get(key)
    console.log(response[0])
}

// get all employees
async function getAllEmployees(){
    const query = datastore.createQuery('Employee')// get all 
    const response = await datastore.runQuery(query)
    console.log(response)
}

// get all employees by age
async function getEmployeesYoungerThan(age = 0){
    const query = datastore.createQuery('Employee').filter('age','<',age)
    const response = await datastore.runQuery(query);
    console.log(response)
}

// get all employees by age
async function getEmployeesBetween(lower = 0, upper = 0){
    const query = datastore.createQuery('Employee').filter('age','<',upper).filter('age','>',lower)
    const response = await datastore.runQuery(query);
    console.log(response)
}

// update you do not update so much as overrite (usually)
// can use merge to selectively update
async function updateEmployee(id, data){
    const key = datastore.key(['Employee',id]);
    const response = await datastore.merge({key:key,data:data})
}

// deleting
async function deleteById(id){
    const key = datastore.key(['Employee',id])
    const response = await datastore.delete(key)
}
