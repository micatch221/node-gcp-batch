import express from 'express';

const app  = express();
app.use(express.json()); // this will allow Node to read the body of HTTP requests as JSON
// Middleware - Software that tweaks how express will handle requests and responses

let counter = 0;

// path, callback function that will be called when a GET HTTP Request is sent to that path
// req: An Object that holds information on the HTTP Request
// res: An object used to generate HTTP Responses
app.get("/hello", (req, res)=>{
    res.send("Hello I just responded to an HTTP request!!!");
});

app.get("/hola", (req, res) =>{
    counter++;
    res.send("Hola Current counter total is " + counter);
});

//:person is a Path Parameter
app.get("/greet/:person", (req,res)=>{
    const person = req.params.person; // ALWAYS A STRING BTW
    res.send("Hello " + person);
    console.log("Hello");
});

app.get("/add/:num1/:num2", (req, res)=>{
    const num1 = Number(req.params.num1); 
    const num2 = req.params.num2 - 0; // Bad btw do not use
    const sum = num1 + num2;
    res.send("Sum " + sum);

});

app.post("/multiply", (req, res)=>{
    const body = req.body; // automatically turns the Reuqest JSON into a JSO
    const num1 = body.num1;
    const num2 = body.num2;
    const product = num1 * num2;
    res.send("Product " + product);
})

app.listen(3000, ()=>{ console.log("The server has started !!!")})