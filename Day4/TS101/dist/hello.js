// TypeScript's biggest feature is that TypeScrit has types
// You can declare variables and functions to be of a certain type
var fname = "Adam"; // Valid JS so also valid TS
var lname = "Ranieri"; // this variable IS a string type
// lname = 90; TS will yell at you
var result = true;
// function takes in a name that is a string and void means there is no return
function greetPerson(name) {
    console.log("Hello " + name);
}
// greetPerson() // TS will yell at out. It will still work as JS but when you transpile it
// the TS transpiler will give error saying this might be a problem
// TS is all about trying to catch errors when the write code. Not when you run the code
// JS would just pass in undefined
greetPerson("Adam");
function add(num1, num2) {
    return num1 + num2;
}
var sum = add(90, 5);
