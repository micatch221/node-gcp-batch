"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var app = express_1["default"]();
app.use(express_1["default"].json());
var associates = [];
// GET: Read
app.get("/associates", function (req, res) {
    res.send(associates);
});
app.get("/associates/:name", function (req, res) {
    var name = req.params.name;
    var filteredAssociates = associates.filter(function (a) { return a.name === name; });
    if (filteredAssociates.length === 0) {
        res.status(404);
        res.send("No associate with that name");
    }
    else {
        res.send(filteredAssociates[0]);
    }
});
// POST: Create
app.post("/associates", function (req, res) {
    var associate = req.body;
    associates.push(associate);
    console.log("HELLO");
    res.send("Added a new associate");
});
// DELETE: delete
app["delete"]("/associates/:name", function (req, res) {
    var name = req.params.name;
    for (var i = 0; i < associates.length; i++) {
        if (associates[i].name === name) {
            associates.splice(i, 1);
            res.send("Deleted the associate");
        }
    }
});
// PATCH modify
app.patch("/associates/:name/adjustpoints/:value", function (req, res) {
    var name = req.params.name;
    var value = req.params.value;
    for (var _i = 0, associates_1 = associates; _i < associates_1.length; _i++) {
        var associate = associates_1[_i];
        if (associate.name === name) {
            associate.points = associate.points + Number(value);
            res.send("The new point total is " + associate.points);
        }
    }
});
// PUT: replacement
// Upsert Replace the associate if it is there. Create a new one if does not exist
app.put("/associates/:name", function (req, res) {
    var name = req.params.name;
    var associate = req.body;
    for (var i = 0; i < associates.length; i++) {
        if (associates[i].name === name) {
            associates[i] = associate;
            res.send("Updated Associate");
        }
    }
});
app.listen(3000, function () { console.log("Server started"); });
