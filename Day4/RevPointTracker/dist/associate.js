"use strict";
exports.__esModule = true;
// export will allow us to use this class in another file
// default means we do not need curly bracket when we import it
var Associate = /** @class */ (function () {
    function Associate(name, points) {
        this.name = name;
        this.points = points;
    }
    return Associate;
}());
exports["default"] = Associate;
