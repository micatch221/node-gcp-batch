
import json # This library turns python dictionaries into JSON

person = {"Name":"Adam", "age":19}

json_person = json.dumps(person)
print(json_person)