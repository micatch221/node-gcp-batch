// You need to be able to react to users.
// They click a button.... something happens
export default function GreetButton(){

    // every event handler has an event object passed as the first parameter
    // react does this for you automatically
    // you might not always use it but I want you to get into that mindset
    function helloPopUp(event = new Event()){
        console.log(event)
        alert("Welcome !!!!!")
    }

    // all JSX elements have optional on"something" attributes
    // these attributes can be linked to a function to call (eventHandler) when that event happens
    // onClick, onMouseOver,
    return(<button onClick={helloPopUp} >Click Me!</button>)

}