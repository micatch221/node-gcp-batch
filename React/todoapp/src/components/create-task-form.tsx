import { useRef } from "react"
import { useDispatch } from "react-redux";
import { Task } from "../stores/task-store";


export default function TaskCreationForm(){
    const taskNameInput = useRef<any>(null);
    const taskPriorityInput = useRef<any>(null);

    const dispatch = useDispatch()// hook that will allow a component to dispatch messages

    function addTask(){
        const taskName:string = taskNameInput.current.value;
        const taskPriority:number = Number(taskPriorityInput.current.value);
        const task:Task = {name:taskName, priority:taskPriority, isComplete:false}
        dispatch({type:"add", task:task})// message to send 
    }

    return(<div>
        <h3>Create Task Form</h3>

        <label htmlFor="taskNameInput"></label>
        <input name="taskNameInput" type="text" placeholder="do dishes" ref={taskNameInput} />

        <label htmlFor="priortiyInput"></label>
        <input name="priorityInput" type="number" placeholder="0" ref={taskPriorityInput} />

        <button onClick={addTask}>Add Task</button>

    </div>)
}