import TaskTable from "./task-table";

export default function TaskViewerPage(){

    return(<div>
        <h1>Task Viewer Page</h1>
        <p>This page will allow you to view and edit tasks</p>
        <TaskTable></TaskTable>
    </div>)
}