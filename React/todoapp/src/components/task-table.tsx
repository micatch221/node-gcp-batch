import { useSelector } from "react-redux";
import { Task, TaskState } from "../stores/task-store"


export default function TaskTable(){

    const storeTasks:Task[] = useSelector((state:TaskState)=>state.tasks);// takes a callback selectorfunction
    // the return of this function is the value stored in the variable
    const taskRows = storeTasks.map(t => <tr>
        <td>{t.name}</td>
        <td>{t.priority}</td>
        <td>{t.isComplete ? "YES": "NO"}</td>
        </tr>)

    return(<table>
        <thead><th>Task Name</th><th>Priority</th><th>Complete?</th></thead>
        <tbody>
            {taskRows}
        </tbody>
    </table>)

}