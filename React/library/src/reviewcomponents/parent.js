import { Provider } from "react-redux";
import store from "../stores/counter-store";
import Child from "./child";



export default function Parent(){
    const adam = "Adam"
    const bill = "Bill"
    const steve = "Steve"

    return(<div>
        <Provider store={store}>
            <Child name={adam}></Child>
            <Child name={bill}></Child>
            <Child name={steve}></Child>
        </Provider>

        
        
    </div>)

}