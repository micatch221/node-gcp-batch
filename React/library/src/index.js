import React from 'react';
import ReactDOM from 'react-dom';
import BookForm from './components/book-form';
import ViewerPage from './components/viewer-page';
import Parent from './reviewcomponents/parent';


ReactDOM.render(
  <React.StrictMode>
    {/* <ViewerPage></ViewerPage>
    <BookForm></BookForm> */}
    <Parent></Parent>

  </React.StrictMode>,
  document.getElementById('root')
);


