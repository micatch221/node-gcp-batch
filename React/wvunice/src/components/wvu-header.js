import AppBar from '@material-ui/core/AppBar';
import  Typography  from '@material-ui/core/Typography';
import BusinessIcon from '@material-ui/icons/Business';


export default function WvuHeader(){

    return(<AppBar>
        <Typography variant='h3'>
            WVU Revature office
            </Typography>
        <BusinessIcon></BusinessIcon>
    </AppBar>)

}