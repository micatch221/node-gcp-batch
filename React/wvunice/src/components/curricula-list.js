import { List, ListItem } from "@material-ui/core";

export default function CurriculaList(){

    return(<List>

        <ListItem>Java Microservices</ListItem>
        <ListItem>Java Automation</ListItem>
        <ListItem>Python Automation</ListItem>
        <ListItem>Node Cloud Developer</ListItem>

        </List>)

}