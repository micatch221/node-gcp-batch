import React from 'react';
import ReactDOM from 'react-dom';
import CurriculaCard from './components/curricula-card';
import CurriculaList from './components/curricula-list';
import Intro from './components/intro';
import InformationForm from './components/more-information-form';
import WvuHeader from './components/wvu-header';

ReactDOM.render(
  <React.StrictMode>
    {/* <WvuHeader></WvuHeader> */}
    
    <Intro></Intro>
    <CurriculaCard></CurriculaCard>
    <InformationForm></InformationForm>
  
  </React.StrictMode>,
  document.getElementById('root')
);
