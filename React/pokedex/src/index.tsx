import React from 'react';
import ReactDOM from 'react-dom';
import Intro from './components/intro';
import PokeLookup from './components/poke-lookup';


ReactDOM.render(
  <React.StrictMode>
   <Intro></Intro>
   <PokeLookup></PokeLookup>
  </React.StrictMode>,
  document.getElementById('root')
);

