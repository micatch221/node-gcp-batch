const {Storage} = require('@google-cloud/storage');
const storage = new Storage();

async function getFile(){
    const bucket = storage.bucket('ranieri-example-bucket')
    const response = await bucket.get('microservices.png')
    console.log(response)
}

// getFile()
// base64 encoded file SGVsbG8hIDop
async function uploadFileMakePublic(){
    const bucket = storage.bucket('ranieri-example-bucket')
    const buffer = Buffer.from('SGVsbG8hIDop','base64')
    const file = bucket.file('uploadedfile.txt')
    await file.save(buffer) // takes time so we should await it
    await file.makePublic()
    console.log(file.publicUrl())

}

exports.upload = async (req,res) =>{
    const body = req.body;
    console.log(body)
    console.log(body.extension)
    const bucket = storage.bucket('ranieri-example-bucket');
    const buffer = Buffer.from(body.content,'base64');
    const file = bucket.file(`${body.name}.${body.extension}`);
    await file.save(buffer);
    await file.makePublic();
    res.send({photLink:file.publicUrl()})
};