# Cloud Functions
- When you write code you have to deploy it.
    - Usually this invovles setting a server of some kind
    - Manually in GCE 
        - create the vms and install everything yourself
    - Managed by Gooogle using App Engine
        - Google will take of a lot the heavy liftings
```txt
Google Cloud Functions is a lightweight, event-based, asynchronous compute solution that allows you to create small, single-purpose functions that respond to cloud events without the need to manage a server or a runtime environment
```
- **Serverless** programming
    - With cloud computing the provider can do a LOT behind the scenes for you
    - Developers want to spend more time *writing* code and less time *deploying*
    - Serverless is the ultimate expression of this paradigm where you can directly deploy functions *without* you ever managing a server.
    - Obviously there are still servers running the code 
        - Completely abstracted away from you.
- **Trigger**
    - Is some event that will cause your function to execute
        - HTTP request
        - File upload to cloud storage
        - pub/sub
- Languages supported
    - JS
    - C#
    - Java
    - Python
    - Ruby
    - Go
    - PHP
    - You cannot write a cloud function in any other language. Pick a different service if you need a language not supoorted