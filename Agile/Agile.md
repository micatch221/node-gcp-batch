# Agile
- A way of thinking
    - More modern approach to software
- A philosphy not a well defined carved in stone rules
- The core belief is in **agility**
    - Respond to change
    - You should avoid rigid unbending plans
- Agile is used outside of software environments
- There are a lot of things *unique* to software developement that Agile is good at adressing.
    - New technologies and practices are alwways arising.
        - A civil construction enginer can always count on physics being the same.
        - Buidlings that have been around a 1000 years before a computer.
    - Rapid prototyping and editing is a luxury that software engineering has.
        - A bridge constructor cannot just edit the bridge real quick then crtl+z if it doesn't work.
            - Time
            - Safety
            - Money
    - Trying to plan how long and how difficult it is the write something is one of the hardest parts of software.
- Agile manifesto guidelines
    - ***Responding to change Over following a plan***
        - The most important part of agile
        - Treat problems individually
    - **Working Demos Over Documentation**
        - Working code > documentation
        - Much better to show off a demo of what you have done then a document that outlines hours worked on what.
    - **Customer Collaboration Over Contracts**
        - You are making for *people*
        - Give the customer what they want 
    - **Individuals and Interactions Over procedure and process**
        - Be a people person
        - Talking with people and coworkers is a lot better than email chains or weekly reports
- Testing and Development are essentially two sides of the same coin
    - They should be done simultaneously
    - Great feedback loop with each other

# Waterfall
- Older approach to software development
- Everything is planned out from the start.
- Everything is well documented and structure
- You move forward and never move back and redesign
- Not inherently *bad*
    - This could be a correct approach for some projects
    - Imagine writing secure software for a military helicopter
        - You know *exactly* the hardware it is running
        - You need 100% certainity that there is no software failures
- ![Agile vs Waterfall](https://www.techguide.com.au/wp-content/uploads/20 20/01/Agile-750x430.jpeg)

# SCRUM
- Scrum is an **implementation** of agile.
    - Kanban
    - Scrumban
- Specifc practices with ceremonies to perform.
- Specifc roles with job responsiblities
- Roles
    - **Product Owner**
        - The person who is ultimately responsible for project
        - Represent the **stake holders** who are the people who commissioned the team for the software
        - **Business Analyst** has some technical background but is not a software developer.
    - **Scrum Master**
        - The person in charge of the software team.
        - Remove obstacles to help the team achieve peak performance
            - Cheerleading
            - Moving people around to better fix problems
            - Helping people communicate with each other
            - Technial expertise
        - Most scrum masters will be senior or lead developer
    - **Scrum Team Member** **Scrumlings**
        - The actual people in a scrum team
- Scrum is Iterative
    - You work on the project in small **sprints**
    - 2-3 weeks in length
- Timeline of ceremonies in a ***Sprint***
    - Day 0: **User Story** grooming and planning
        - Creating user stories and assigning them
    - Day 1-14: Daily Standup
        - Every team member will give an update on how they are handling their tasks.
            - Overall Progres
            - If you need help with anything
            - If you have the time to help someone else
    - Day 14: Sprint Review
        - Collate all of your results
        - Get the last of the code into repositories
        - Get demos up and running
        - Present to the stake holder
    - Day 15: Sprint Retrospective
        - Internal team review
        - Discuss what went well and what did not
        - Lessons learned
        - Ideas for the next sprint
    - Backlog Refinement
        - Updating user stories and reassigning them
![Sprint Timeline](https://www.parabol.co/hs-fs/hubfs/The%205%20Scrum%20Ceremonies%20in%20order.png?width=1518&name=The%205%20Scrum%20Ceremonies%20in%20order.png)
### User Story
- In most agile approaches (scrum) you approach building software from the perspective of User Stories
- A combination of a
    - Type of user
    - What the user wants to do
    - Why They want to do it
    - Pointing 
        - Allocation of the difficulty to complete the user story
- Try avoiding as a user
    - Becuase literally everyone is a user of the application
    - There is almost always a more descriptive title you could give
- Imaginary user stories for Joes Pizza
|As a|I Want to |So That|points|
|----|-----------|--------|----|
|Employee| View pending orderings | I can get started on the orders as soon as they come in| 5 |
|Customer| Save my orders | I can quickly reorder my favorites in the future| 8 |
#### Pointing
- This is the process of assinging difficulty to a user story
    - Some people do not use points
        - x-small,small,medium, large, x-large
    - Fibonacci numbers
        - 1 2 3 5 8 13
            - If you have numbers greater than 13 you need to break the user story into smaller parts
    - Most teams will have a user story with an agreed upon difficulty and base their points relative to it
        - A user story for filtering pizzas by when they were ordered
            - 5


    

    