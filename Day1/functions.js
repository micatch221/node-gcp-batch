// functions are reusable chunks of code in our program

// defines a function
function sayHelloWorld(){
    console.log("Hello World");
}

function greetPerson(name){
    console.log("Hello " + name)
}

// can't enforce the types of parameters
function multiGreet(name, times){

    for(let i = 0; i<times;i++){
        console.log("Hello " + name)
    }
}

multiGreet("Adam", 2);
// JS is VERY leinent in accepting mistakes and not throwing errors
// A lot of developers find this behavior unintuitive and can make writing JS a bit more cautious

greetPerson("Hank");
greetPerson(); // Not an error, JS will pass in the value undefined for the parmater if you never passed on in
multiGreet(0,2,"Hello",true) // works FINE 0 errors  when you run the code
// JS will just ignore the extra parameters
// JS does not support overloading
