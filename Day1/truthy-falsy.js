// JS has extremely aggressive type coercion
// JS will force a value of one type into another type implicty
// JS will compare apples to oranges even if they are both bananas

// Every value in JS has an implict "truthiness" associated with it

// console.log(100 == true) // false
// console.log(1 == true) // true
// console.log('' == 0) // true
// console.log("hello" == 50) // false
// console.log("50" == 50) // true
// console.log(100/"Tim" == false)// false

// const result = 100/"Hello";
// console.log(result)// NaN Not A Number
// console.log(typeof result) // NaN is of type number

// console.log(NaN == result)// false
// console.log(result == result) // false
// // The comparisons are not even transitive due to coercion rules and priorties
// console.log("0" == 0)
// console.log(0 == false)
// console.log("-0" == false)


// There are certain FALSY values which you must know!
// If you were to put coerce these values into a boolean they would be 
// the falsy values kinda imply non-existence
console.log(Boolean("")) // empty string
console.log(Boolean(0)) // zero
console.log(Boolean(null))
console.log(Boolean(undefined))
console.log(Boolean(NaN))
console.log(Boolean(false))

// Everything else is true

if(""){
    console.log("in if")
}else{
    console.log("Hello")
}