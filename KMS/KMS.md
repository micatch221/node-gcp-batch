# Key Management Service
- You will have files that contain sensitive information and you want to make sure only certain people can read the contents
- AES
    - Advanced Encryption Standard
    - The most common encryption process in the world*
        - *kinda a guess
- **Encryption** is the process of taking data and putting into a secure format so that only authorized people can decrypt to read it
- Two main types of encryption
    - **Symmetric Encryption**
        - A single key used to *encrypt and decrypt* information
    - **Asymetric Encrption**
        - One key is used to encrypt
        - Second key is used to decrypt
```bash

gcloud kms encrypt --location=global --keyring=revapoint-keyring --key=revapoint-key --plaintext-file=dbconnection.txt --ciphertext-file=encryptedinfo.txt

gcloud kms decrypt --location=global --keyring=revapoint-keyring --key=revapoint-key --plaintext-file=decryptedfile.txt --ciphertext-file=encryptedinfo.txt
```