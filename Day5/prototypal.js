// JS did not have classes for a long time BUT it did have inheritence
// Objects could point to a prototype object
const person = {
    fname:"Adam",
    describeSelf(){
        console.log("Hi my name is " +this.fname);
    }
}

const bill = {fname:"Bill"}
bill.__proto__ = person;// All objects have a hidden __proto__ property that references
// another object. You can inherit properties
bill.describeSelf()

function hello(){
    const kim = {fname:"Kim"}
    console.log("Hello")
}

hello();
hello();
hello();
