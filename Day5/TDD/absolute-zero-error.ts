export default class AbsoluteZeroError {
    message:string;

    constructor(message:string){
        this.message = message;
    }
}