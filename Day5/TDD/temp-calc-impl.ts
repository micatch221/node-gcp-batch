import AbsoluteZeroError from "./absolute-zero-error";
import TemperatureCalculator from "./temp-calculator";


export default class TempCalcImpl implements TemperatureCalculator{

    convertTemp(from: string, to: string, temp: number, precision?: number): number {

        if(from.toUpperCase() === "F" && temp < -273){
            throw new AbsoluteZeroError(`Input temperature of ${temp} is below absolute zero of -273 F`)
        }

        if(from.toUpperCase() === "F" && to.toUpperCase() === "C"){
            const celcius = (temp -32) * 5/9;
            return Number(celcius.toFixed(2));
        }
        if(from.toUpperCase() === "C" && to.toUpperCase() === "F"){
            const farenheit = (temp*9/5) + 32;
            return Number(farenheit.toFixed(2));

        }

    }
    isFreezing(from: string, temp: number): boolean {
        throw new Error("Method not implemented.");
    }
    isMelting(from: string, temp: number): boolean {
        throw new Error("Method not implemented.");
    }
}

