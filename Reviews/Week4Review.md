# Week 4 Review

# Networking
- IP address
    -  Intenet Protocol Address
    - Unique Identifier number for a device connected to the internet
- IPv4
    - The original IP address.
    - Usually what we are referring to when we say IP address
- IPv6
    - A newer set of IP addresses
    - Designed to address that we are running out of IPv4 addresses
    - Particualry true for the internet of things
- IP addresses are acutually in binary
- They are 4 bytes usually seperated by a dot
- Usually written in deciaml for readability purposes
    - 10011011.10110111.10101101.10011110
    - 230.19.40.4
    - All ip Addresses are between the values of 0-255 for every quad
- **CIDR** Classless Inter-Domain routing notation
    - Way of specifying a *range* of IP addresses
    - 12.1.65.0/24 Specifying the IP ranges between 12.1.65.0 - 12.1.65.255
    - the /24 indicates that the first 24 bits are fixed
- **Static IP Address** 
    - An IP address that does not change once assigned
- **Ephemeral IP Address**
    - An IP address that *might* be reassigned/changed when your vm or other cloud resources is shutdown or restarted
# Ports
- To connect to a computer you need IP and a **Port**
- Access point on the computer 
- Standard ports
    - HTTP 80
    - HTTPS 443
    - SSH 22
    - SMTP 25

# SSH
- Secure Shell
- Main protocol for connecting to linux computers
- It operates on a key based system

# Linux
- It an open source operating system
- Very popular for web servers
    - Free with no licensing restrictions
    - Very efficient and minimalist. Run more applications less CPU and RAM used by OS
    - A lot of great software tools already developed for Linux
- Tons of linux distros
    - Debian is the one we used
    - The original linux designed by Linus Torvalds
- Most Linux distros DO NOT have a graphical user interface GUI
    - Everything is done in terminal
- Linux originally comes from Unix
- Unix was designed for mainframe computers
    - Many users using one machine
    - User permissions are in built to most linux distros
    - A lot of the time I have to do sudo to act as a *root* user who has full access
```bash
# common linux commands
ls # list 
cd # change directory
pwd # print working directory
sudo # super user do
cp # copy paste
mv # move
rm # remove
mkdir # make directory
touch # make a file
cat # print out contents of a file
vim # open the text editor
ps # show currently running process
kill # killing a process
bg # background
chmod # change modification (editing permissions on a file)
```
- Linux is like JSON or JS
    - You better get used to it and learn it
    - It is an essential part of every web developer's skill set

# GCP
- Google Cloud Platform
- A cloud service provided by Google
    - If you do not know about google, google them
- 3rd largest cloud
![Google routing](https://cloud.google.com/images/locations/edgepoint.png)
- One of GCP's points of pride is its global network
- All other computing centersaround are connected via optic cables
    - All traffic within GCP does not have to actually leave the google network and use public internet cables
    - Traffic can flow very fast and optimized within GCP
- **Premium Routing**
    - Pay more for it
    - Your end users will get connected to GCP hosted applications at the nearset possible data center.
    - All traffic within GCP is intenal
- **Standard Routing**
    - Pay less
    - Your users will get connected to the data center nearest the application
![Types of routing](routing.png)

# GCP Infrasturcture
- **Regions**
    - Geographic areas like us-east1 (South Carolina)
- **Zones**
    - Zones are locations within region
    - Most region have several zones (us-east1-a , us-east1-b)
    - This provides redundancy in case there is a failure
    - One has the power knocked out from a storm
- **Data centers**
    - The physical buildings that house the IT infrastructure
    - Server racks and cables
    - GCP purposefully seperates employees who work on the hardware vs the software for GCP

# GCE
- Google Compute Engine
- One of the most essential GCP services
- Allows you to create VMs in the cloud
    - These vms are called instances
- Things you can customize about a VM
    - Operating system
    - Where it is physically located
    - The boot disk
    - The processors and RAM for the machine
    - Service Account (we have not covered service accounts)
        - The permissions that the VM is allowed
    - What network you want the VM on
    - The name
    - Meta Data
        - Information about the VM
    - Startup script
        - A script to run whenever the VM is first created or restarts
- **Instance Template**
    - Saved confifuration of a VM creation form
    - Primarily to quickly create a specific VM
- **Persistent Disks**
    - All vms are creted with a persistent disk
    - hard drive for a VM
        - SSD
        - Disk Drive
    - They get the term persistent disk becuase they ARE NOT attached to the vm
    - They live seperately from the VM
    - And can surver the vm being shutdown
    - They can be copied
        - **Snapshots**
    - They can be remounted onto other VMs
- **Instance Group**
    - A group of instances that you have logically lumped together
    - **Unmanged Instance Groups**
        - VMs manually added to a group
        - They are unmanaged in that the developer has to do any maitnence manually 
            - adding new computers 
            - removing new vms
            - running updates on all the vms
        - These vms *DO NOT* have to be identical
    - **Managed Instance Group**
        - All vms must be created from the same template
        - You can configure GCP to *manage* them
            - Unifmormally update
            - Unifomrally add and remove vms
# Elasticty 
- One of the biggest benefits of cloud computing is how quickly you can create or delete resources to match your current level of demand.
- You only provision what you need at that moment
- **Auto-Scaling**
    - The ability of your online applications to provision or decomission resources to meet demand
    - **Vertical Scaling**
        - You get a more powerful vm to meet more demand
            - 1 2cpu 2 gbs vm => 1 8cpu 32 gbs vm
    - **Horiztonal Scaling**
        - You get more vms of the same technical stats and split traffic between them
        - 2 2cpu 2 gbs vms => 8 2cpu 2 gbs vms
        - Requires a load balancer
        - Usually preferred for web applications
- **Elasticity**
    - The speed at which your application auto-scale

# Load Balancer
- A load balancer serve as an entry point for requests going to your vms
- Reasons to use a load balancer
    - Split the traffic to prevent a single VM from getting overloaded
    - Protect you from DDOS attacks (someone tries to send a ton of requests to overload your servers)
    - Can check the credntials of the requests to block unathourized requests
    - They perform health checks on vms to make sure they are up and running and only directs traffic to healthy vms
    - Automatically provision or decomissions vms to handle the current traffic load
    - Load Balancers often have a static IP which means there is a nice fixed address for your application

# Cloud VPC
- All vms and resources must be deployed to a **Virtual Private Cloud**
- A VPC is a encapsualted networking environment for your resources
- A VPC is a **GLOBAL** construct
- You can set up **Firewall Rules** for your VPC
- There is a defulat VPC created with every GCP project with default firewall rules
    - You can create your oewn
- Parts of a Firewall Rule
    - **Name**
        - Human readable description
    - **Type**
        - ingress (traffic going *into* the network)
        - egress (traffic going *out* of the the network)
    - **Target**
        - All computers on the network
        - Specific target tag
        - Service Account
    - **Filter**
        - IP Range
        - Service Account
        - specific tags
    - **Port**
    - **Action**
        - Allow (The rule is granting the permission to do something)
        - Deny (The rules prohibits)
    - **Priority**
        - number between 0 - 65,535
        - Lower numbers take precedence over higher numbers
        - if a rule with a priortity of 1000 allows traffic from anywhere on port 8000 it will override a rule with priority 5000 blocking traffic from everywhere.
- **Subnet**
    - Range of IP address within a network
        - usually done for security or to optimize traffic flow
        - computers on the same subnet can communicate faster with each other
    - In GCP subnets are for a specific *region*
        - east-asia-1 subnet 
        - europe-2 subnet

# Cloud Storage
- **Object Based** storage
    - files
    - blob (binaray large object)
    - jpgs, pngs, pdfs, txts, htmls
- Very convient for storing any files an application needs to save
- I reccomend storing any files in Cloud Storage and then saving a link to that file in your database.
- Objects go into **buckets**
    - buckets must be globally unique
    - Uniquely named across ALL GCP accounts created by everyone
- Bucket permisions
    - Uniform
        - All objects in the bucket have the same permission level
    - Fine Grained
        - You can set persmission levels per object
- **Static Website Hosting**
    - Putting the HTML/CSS/JS into a bucket and making the files public
- **Storage Classes**
    - Standard
        - Storing objects that are frequently accessed and updated.
        - files that people commonly download.
        - or files for a public website.
    - Nearline
        - 30 days is the minimum time objects in nearline must be stored or you get charged extra
        - Monthly business reports that you might access infrequently.
    - Coldline 
        - 90 days is the minimum time objects in coldline must be stored or you get charged extra
        - Quarterly business reports. Local news company might have some unused video that goes unused but do not want to get rid of.
    - Archive
        - 365 days is the minimum time objects in archive can be store or you get charged.
        - For files that you do not anticpate needing at all or very very infrequently
        - Legally required to keep medical examination documents for 10 years.
- The cost per gigabyte of storage goes DOWN. 
- The cost of accessing each object goes UP.

# Cloud Monitoring
- GCP can monitor your vms and other cloud resources
- Monitoring vs Logging
    - Monitoring
        - Metrics
        - CPU usage
        - Memory 
        - Incoming Trafficing
    - Logging
        - Text based messages
        - History of events
            - Created a VM at 7:18 PM EST
            - Delete VM at 9:23 PM EST
            - Sudden Crash VM at 11:39 PM EST
- There is a monitoring console that you can accesss on the GCP website
- In order to view some metrics on your VM you will have to install monitoring agents on your vm    
    - It is just a button that gives you a command to run
# StackDriver
- Was an independent company that made monitoring and logging software
- Google bought StackDriver and incorporated it into GCP
    - StackDriver logging
    - StackDriver monitoring
    - StackDriver error Reporting
- GCP rebranded all the Stackdriver stuff to Cloud
    - Cloud logging
    - Cloud  monitoring
    - StackDriver Error Reporting

# Cloud Shell
- Online terminal 
- Connects to a personal VM
- Any files that you write here are persisted and saved acorss projects
- 100% a developer tool to help you perform GCP operations
    - Text based

# Cloud SDK
- Local **Software Development Kit**
- Cloud shell but on your local computer
- Text based
- Very helpful for writing commands and using them on your local machine














