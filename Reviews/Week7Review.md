
# Week 7 Review

## Docker
- Containerization software

- pros
    - Makes your applications very portable and easy to deploy
        - Linux Process on a computer
        - No need to manage the container environment, like runtime or dependencies
    - containers are faily lightweight. Easy to spin up and destroy
        - great for load balancing.
- con
    - All virtualization will incur a small proccessing cost
    - Not great for applications that need local persistent storage
        - It possible. I wouldn't reccomend it for the most part.
- Containers vs VM
    - Containers
        - Lightweight
        - Process/program running on a machine
    - VMs
        - Fully fledged machines
        - Contain an Operating System
![container vs vms](https://images.contentstack.io/v3/assets/blt300387d93dabf50e/bltb6200bc085503718/5e1f209a63d1b6503160c6d5/containers-vs-virtual-machines.jpg)
- Key Docker Terminology
    - **Dockerfile**
        - A script that creates an image
    - **Image**
        - The blueprint for a container
        - A Class in OOP
        - Contains all the information necessary to create a container
            - runtime environement
            - files
            - startup command
    - **Container**
        - A running application/program
        - Isolated and self contained
        - An object OOP
        - You can have many running containers of the same image
    - **Dockerhub**
        - A respository for images
        - GCP has its own version called **GCR** Google Container Registry

```dockerfile
FROM node 
# FROM base image the foundational back software that will be in the image and containers created from this image
# any image created from the Dockerfile WILL include node.js
COPY . /workspace
# COPY source destination  . means everything in the current directory
# source path to the local folder, Destination the file directory name in the container

WORKDIR /workspace
# The commands you run in your dockerfile. What directory in the container do you want to run them from?

EXPOSE 3000
# port 3000 on a container created from the image is accessible
RUN npm install
# RUN is used for commands to execute when creating the image

# a command to run when you create a container based off of this images
ENTRYPOINT [ "node", "index.js"]

```
```bash
# -t tag (name of the image)
docker build -t my-image-name locationofdockerfile
docker build -t bonjour-v1 . # current directory
# build will create an image

docker run image
dcocker run bonjour-v1
# create a container based off of that image
docker run -p 4444:3000 bonjour-v4
# -p publish forward any requests to localhost:4444 to port 3000 on the container 
# the first number is any number that is convienent, the second number must be the port you exposed

docker run -d -p 4444:3000 bonjour-v4
# -d detach run the container in the background. Do not attach the output to my current terminal/powershell

docker push bonjour-v4

docker kill containerid

docker images # lists all images

docker ps # all running containers

docker  --env="SOMETHING=hello"-d -p 5555:3000: bonjour-v4

docket tag orginal-image-name new-image-name

docker pull someimage

```

## Cloud Build
- GCP's service for building images
- Rather than building images locally you can have cloud build construct your images from source code
- Many services used Cloud Build behind the scenes

## Cloud Run
- Serveless container hosting
- Cloud Functions for a container rather than a specific function.
- A cloud service can scale containers from 0 - ALOT to hanlde increases or decreases in traffic
- You have specify an image to use for a service in cloud run

# Kubernetes
- *Container Orchestration*
    - Software that manages your containers.
        - Restarting failed containers
        - Auto-scaling
        - Exposes your containers
- Kubernetes Infrastructure terms
    - **Cluster**
        - A collection of nodes
        - Controlled by a single 'brain' called the **control plane**
    - **Node**
        - A VM that is being controled by the control plane
        - A node must be running a client software called **kubelet** which connects the node to the **control plane**
    - **Control Plane**
        - The brain of a kubernetes cluster
        - Manages the vms. Manages the pods and resource allocation on the vms
    - **Node Pool**
        - A cluster can be comprised on multiple node pools
        - A smaller sub collection of nodes
            - Usually node pools are vms of a same type
        - You might have pods that run on a specific node pool if those pods require something specific from those vms.
            - A node pool might be comprised of high memory vms which is useful if you have pods that need that hardware
![Kubernetes Infrastructure](basic-kubernetes.png)
- Kubernetes Objects
- An object is an abstract representation of a resource
- **Pod**
    - Smallest atomic processing unit in Kubernetes
    - Consist of 1 - many containers
    - Isolated environments
        - Not directly accessible
    - Your applications run in pods
- **Service**
    - Pods cannot be reached directly
    - A service will ***EXPOSE*** a pod or pods 
    - Types of services
        - **LoadBalancer**
            - Create an external Load Balancer 
                - External IP address
            - This will make your pods accessible to the outside internet
        - ClusterIP
            - Internal load balancer
            - Outside traffic cannot read a ClusterIP service
            - intra-cluster communication
        - NodePort
- **Secret**
    - Allows you to store senstive information
    - You can reference these secrets in your other resources/objects
- **Workload**
    - A combination of pod and configuration
    - Always have 4 of pod A
    - **Deployment**
        - For stateless pods
            - Pods that do not use local persistence
    - **Stateful Set**
        - For stateful pods
            - Pods that do have local persistence
    - **Replica Set**
        - Deprecated for the most part
        - K8s documentation says to use Deployment instead for almost all use cases
- **Service Account**
    - Kubernetes Service Account
    - Can be referenced by pods to gain credentials to cloud services
    - KSAs are not specifically for GCP