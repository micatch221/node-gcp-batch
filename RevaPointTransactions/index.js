const express = require('express');
const cors = require('cors');
const axios = require('axios');

const app = express();
app.use(cors());
app.use(express.json())

const transactionHistory = []

app.get('/deductpoints/:email/:amount',async (req, res)=>{
    const response = await axios.get(`https://reva-point-tracker-dot-reva-points-tracker-ranieri.ue.r.appspot.com/associates/${req.params.email}`)
    const associate = response.data[0];
    associate.points -= Number(req.params.amount);
    console.log(associate)
    const putResponse = await axios.put(`https://reva-point-tracker-dot-reva-points-tracker-ranieri.ue.r.appspot.com/associates/${req.params.email}`,associate)
    transactionHistory.push({fname:associate.fname, lname:associate.lname, type:'Deduction',quantity:Number(req.params.amount)})
    res.send('Successfully deducted points')
});


app.get('/addpoints/:email/:amount',async (req, res)=>{
    const response = await axios.get(`https://reva-point-tracker-dot-reva-points-tracker-ranieri.ue.r.appspot.com/associates/${req.params.email}`)
    const associate = response.data[0];
    associate.points += Number(req.params.amount);
    const putResponse = await axios.put(`https://reva-point-tracker-dot-reva-points-tracker-ranieri.ue.r.appspot.com/associates/${req.params.email}`,associate)
    transactionHistory.push({fname:associate.fname, lname:associate.lname, type:'Addition',quantity:Number(req.params.amount)})
    res.send('Successfully added points')
});

app.get('/history', async (req,res)=>{
    res.send(transactionHistory)
})

const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>console.log('App started'));