Brystan Baeder
- helped phong with issue analysis
- helped debug issues that arose in frontend
- spent most of the time debugging
Phong Vo
- set up kubernetes for issue analysis service
- PVC and Workload implemented and tested
Alejandra Cajiao
- got issues from datastore to populate on frontend
- highlight and reviewed update datastore and rerender on click
- changed *aesthetics* on frontend
Thomas Smithey
- implemented headers for authorization
- Finished kubernetes for scheduler
- including secrets
Group Tasks
- test kubernetes implementations
- connect scheduler service to frontend
- add filter options to the frontend

standup 9/30/21

Yesterday, we finished the backend of the town meeting scheduler, deployed it and added in a secret for the database password.  We still have to add logging and the council member password, but we don't think that this will take very much time.  Along with this, the issues/report route was debugged and redeployed for the issue analysis.  We all have experince in the docker/kubernetes deployment pipeline.  For today, the plan is that Jesse and Willis will finish up whats left to do on the town meeting scheduler, and Aldrin and Michael are going to connect the front end framework to the backend and hopefully get it all connected by the end of today.

# 9/30 Stand up Group 5

## Michelle

    - Current: Spent time working along side Charles with understanding Kubernetes. A lot of time messing with the view issues page and playing with hooks and diving deep into understanding React
    - Goals: Go through questions about kubernetes, finish up the react frontend for issue-analysis

## Josh

    - Current: Mainly worked on the frontend for the meeting services mainly done. 
    - Goals: Just needs to implement the delete and update for frontend and will then be completed.

## Rafael

    - Current: Working along side charles with understanding Kubernetes, working on the authorization and looking at best practices. Cleaned up from React end.
    - Goals: Implement authorization at base level and attempt playing with JW

Camden - Added Material-UI to frontend for Town Meeting Scheduler
James - Deployed .yaml files for Issue Analysis and implemented patch routes
Alan - Assisted with Pub/Sub and K8s configurations
Jarrick - Updated frontend styling for Issue Analysis and made edits to Town Meeting Scheduler frontend

## Charles

    - Current: I went over kubernetes as a whole for deploying our backend (excluding pub/sub). Able deploy all services to load balancers initially and eventually set up using ClusterIPs and ingress.
    - Goals: Discuss and implement ingress authorization with Rafa and go over with group

# Overall

    - Goals for today: Iron out everything that's running on the backend and frontend.
    - Challenges: Make it look pretty and work on each member being able to present on all aspects of our project.
