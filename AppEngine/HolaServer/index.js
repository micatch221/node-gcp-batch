const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors())

let counter = 0;

app.get('/hola',(req,res)=>{
    res.send(`Hola version 4.0 you are visitor ${++counter}`)
})


const PORT = process.env.PORT || 3000;// use the port specified by an environment variable called PORT
// if it is not there use port 300
app.listen(PORT,()=>console.log(`Application Started on port ${PORT}`));