# MicroServices
- A **monolithic application** Monolith
    - An application where all of the code is a single program.
    - Your application might have a whole bunch of different services and they are all the same program
    - For Example a pizza Website
        - A Pizza Ordering Service
        - New customer account creation service
        - Employee Login
        - Coupon Redemption service
    - Problems with monolithic Applications
        - They do not scale well
            - Small applications can scale VERY FAST in the cloud
            - Monoliths Scales uniformly regardless of which service is actually being used
                - Recreate the whole application even for services that are barely used
        - They can be difficult to build new features for
            - The application might be using old libraries and frameworks that are cumbersome
            - The developers who worked on the original monolith are no longer avaiable to help
            - It is a lot more difficult to work on an application that has 10,000's of lines of code.
            - Ties you to a specific language for a project
- A **Microservices Application**
    - Desigined to address the problem of a monolith
    - Rather than services being all part of the same program 
    - Each service is its OWN program and they communicate via HTTP
    - Beacuse the services are isolated they can scale independently
    - A crash in one service does not make the entire project unusable
    - Each service is smaller and can scale faster
    - You can write programs in many different languages
        - HTTP and JSON language agnostic


